<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Quick start. Public embedded application</title>
	<script src="//api.bitrix24.com/api/v1/"></script>
</head>
<body>
<?
dp($_REQUEST);

$arTmp = json_decode($_REQUEST["PLACEMENT_OPTIONS"], true);
dp($arTmp);
$grid = $arTmp["GROUP_ID"];
	
if($grid)
{
	$numpage = 1;
	$arBase = array();
	$timest = 0;
	$arParams = array(
		"ORDER" => array("DATE_START" => "asc"),
		"FILTER" => array("GROUP_ID" => $grid),
		"PARAMS" => array('NAV_PARAMS' => array("nPageSize" => 2, 'iNumPage' => $numpage)),   //50 - max enabled
		"SELECT" => array("ID", "TITLE", "GROUP_ID", "PARENT_ID", "TIME_SPENT_IN_LOGS", "TIME_ESTIMATE", "DATE_START", "DURATION_FACT", "DURATION_PLAN", "DURATION_TYPE", "TAGS", "DEPENDS_ON"),
		//"SELECT" => array("*"),
	);
	do
	{
		$arParams["PARAMS"]["NAV_PARAMS"]["iNumPage"] = $numpage++;
		$res = restimpl("task.item.list", $arParams);
		#dp($res);
		foreach($res["result"] as $val)
		{
			unset($val["ALLOWED_ACTIONS"]);
			$timest += $val["TIME_ESTIMATE"];
			$arBase[] = $val;
		}
	}
	while(array_key_exists("next", $res));
	echo "<b>Суммарное планируемое время по задачам: ".secToH($timest)."</b><br>";
	dp($arBase);
}
else echo "Error: В запросе отсутствует ID проекта";

/*
if($arTmp["ID"])
{
	$res = restimpl("task.item.getdata", array($arTmp["ID"]));
	dp($res);
}
else echo "Error: В запросе отсутствует ID задачи";
*/
?>
<script>
BX24.ready(function()
{
	BX24.fitWindow();
	//BX24.init(function(){});
});
</script>
</body>
</html>

<?
function dp($arDamp) {echo "<pre>"; print_r($arDamp); echo "</pre>";}

function secToH($seconds)
{
	$minutes = floor($seconds / 60);
	$hours = floor($minutes / 60);
	$minutes = $minutes - ($hours * 60);
	if($hours < 10) $hours = substr($hours+100, 1);
	if($minutes < 10) $minutes = substr($minutes+100, 1);
	return "$hours:$minutes";
}

function  restimpl($rest, $params="", $prefix="json")
{
	$domain = $_REQUEST['DOMAIN'];
	$auth = $_REQUEST['AUTH_ID'];
	if($params)
	{
		if(is_array($params)) $params = http_build_query($params);
		$params = "&".$params;
	}
	if($prefix == "json")
	{
		$queryData = http_build_query(array("auth" => $auth)).$params;
		$queryUrl = "https://$domain/rest/$rest.$prefix";
	}
	else
	{
		$queryData = "";
		$queryUrl = "https://$domain/rest/$rest.$prefix?auth=$auth$params";
	}

	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => 0,
		CURLOPT_POST => 1,
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $queryUrl,
		CURLOPT_POSTFIELDS => $queryData,
	));

	if($prefix == "json") $out = json_decode(curl_exec($curl), true);
	else $out = curl_exec($curl);
	//else $out = file_get_contents($queryUrl);
	curl_close($curl);
	return $out;
}

?>